FROM debian:buster-slim
MAINTAINER Ruslan Diadin <russtlt@gmail.com>

RUN apt-get update && \
    apt-get -y install wget gcc make libpcre3-dev zlib1g-dev  && \
    cd /usr/src && \
    wget https://nginx.org/download/nginx-1.18.0.tar.gz -O - | tar -xz && \
    cd nginx-1.18.0 && \
    ./configure --with-http_stub_status_module \
                --prefix=/etc/nginx \
                --sbin-path=/usr/sbin \
                --conf-path=/etc/nginx/nginx.conf \
                --error-log-path=/var/log/nginx/error.log \
                --http-log-path=/var/log/nginx/access.log \
                --pid-path=/var/run/nginx.pid \
                --lock-path=/var/lock/nginx.lock && \
    make && make install && \
    cd .. && rm -R nginx-1.18.0 && \
    apt-get -y remove wget gcc make && \
    apt-get clean && \
    apt-get -y autoremove

ADD nginx.conf /etc/nginx/nginx.conf

EXPOSE 80 8080 443

CMD ["/usr/sbin/nginx", "-g", "daemon off;"]
